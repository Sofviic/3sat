module Main (main) where

import Control.Monad
import Data.Maybe
import Text.Read

type SATVar = Int
type SATLit = (Bool, SATVar)
type SATClause = (SATLit, SATLit, SATLit)
type SATForm = [SATClause]
type SATSolution = ([SATVar],[SATVar]) -- ([FALSE required],[TRUE required])

main :: IO ()
main = do putStrLn "Hello World!"
          text <- readFile "src/roussel1.sof.3sat"
          let form = satFileParse . lines $ text
          let highestVar = maxVar form
          let combs = sequenceA . flip replicate [0,1] $ highestVar
          let ios = do 
                        combR <- combs
                        let comb = reverse combR
                        [putStrLn "-----------------",
                         putStrLn $ "Trying " ++ show comb,
                         print . satcheck form $ ([1..highestVar] \\ unboolify comb,unboolify comb)]
          sequence ios
          return ()


maxVar :: SATForm -> SATVar
maxVar []     = 0
maxVar (c:cs) = max (maxVar' c) $ maxVar cs

maxVar' :: SATClause -> SATVar
maxVar' ((_,a),(_,b),(_,c)) = maximum [a,b,c]

unboolify :: [Int] -> [SATVar]
unboolify = catMaybes . zipWith (\i b -> if b == 1 then Just i else Nothing) [1..]

satFileParse :: [String] -> SATForm
satFileParse = catMaybes . fmap satFileParseLine

satFileParseLine :: String -> Maybe SATClause
satFileParseLine s = do guard $ length (words s) > 2
                        let (a:b:c:_) = words s
                        xa <- satFileParseLit a
                        xb <- satFileParseLit b
                        xc <- satFileParseLit c
                        return (xa,xb,xc)

satFileParseLit :: String -> Maybe SATLit
satFileParseLit s = do guard $ length s > 0
                       let neg = '!' == (s !! 0)
                       lit <- readMaybe $ dropWhile (== '!') s
                       return (neg,lit)

satOutFile :: SATSolution -> [String]
satOutFile (fs,ts) = sortOn (dropWhile (== '!')) 
                        $ fmap (("!"++) . show) fs ++ fmap show ts

satcheck :: SATForm -> SATSolution -> Bool
satcheck [] _ = True
satcheck (c:cs) sol = satcheckclause c sol && satcheck cs sol

satcheckclause :: SATClause -> SATSolution -> Bool
satcheckclause ((neg1,x1),(neg2,x2),(neg3,x3)) sol =
    x1 `elem` (if neg1 then fst else snd) sol ||
    x2 `elem` (if neg2 then fst else snd) sol ||
    x3 `elem` (if neg3 then fst else snd) sol

sortOn :: Ord b => (a -> b) -> [a] -> [a]
sortOn _ []     = []
sortOn f (x:xs) = lesser ++ [x] ++ greater
                where 
                    lesser  = sortOn f . filter (\e -> f e <= f x) $ xs
                    greater = sortOn f . filter (\e -> f e >  f x) $ xs

(\\) :: Eq a => [a] -> [a] -> [a]
[]      \\ _  = []
(x:xs)  \\ y  = (if x `elem` y then [] else [x]) ++ (xs \\ y)
